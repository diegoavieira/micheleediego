# Michele e Diego

[![web-app](https://img.shields.io/badge/demo-michelediego.com.br-yellowgreen)](https://michelediego.com.br)
[![pipeline status](https://gitlab.com/diegoavieira/adsystem/badges/master/pipeline.svg)](https://gitlab.com/diegoavieira/adsystem/commits/master)
[![coverage report](https://gitlab.com/diegoavieira/adsystem/badges/master/coverage.svg)](https://diegoavieira.gitlab.io/micheleediego/coverage/web-app)
[![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg)](http://commitizen.github.io/cz-cli/)
[![lerna](https://img.shields.io/badge/maintained%20with-lerna-cc00ff.svg)](https://lernajs.io/)

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.12.

The API was generated with [Firebase](https://firebase.google.com/).

## Requirements

```
npm install -g @angular/cli@9
```

## Config

Create `.env` file at the project root

```
API_KEY=
AUTH_DOMAIN=
DATABASE_URL=
PROJECT_ID=
STORAGE_BUCKET=
MESSAGING_SENDER_ID=
APP_ID=
MEASUREMENT_ID=
```

```
npm run web-app-config
```

## Development

```
npm run web-app-serve
```

## Build

```
npm run web-app-build
```

## Test

```
npm run web-app-test
```
