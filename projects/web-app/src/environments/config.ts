const { writeFile } = require('fs');

require('dotenv').config();

const targetPaths = [
  {
    path: './projects/web-app/src/environments/environment.ts',
    prod: false
  },
  {
    path: './projects/web-app/src/environments/environment.prod.ts',
    prod: true
  }
];

const processEnvs = `
  firebase: {
    apiKey: '${process.env.API_KEY}',
    authDomain: '${process.env.AUTH_DOMAIN}',
    databaseURL: '${process.env.DATABASE_URL}',
    projectId: '${process.env.PROJECT_ID}',
    storageBucket: '${process.env.STORAGE_BUCKET}',
    messagingSenderId: '${process.env.MESSAGING_SENDER_ID}',
    appId: '${process.env.APP_ID}',
    measurementId: '${process.env.MEASUREMENT_ID}'
  }
`;

targetPaths.forEach((e) => {
  const envConfigFile = `
    export const environment = {
      production: ${e.prod},
      ${processEnvs}
    };
  `;

  writeFile(e.path, envConfigFile, (err) => {
    if (err) {
      throw console.error(err);
    } else {
      console.log(`Angular file generated correctly at ${e.path}`);
    }
  });
});
