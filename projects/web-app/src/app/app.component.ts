import { Component, HostListener } from '@angular/core';

@Component({
  selector: 'web-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  @HostListener('window:beforeinstallprompt', ['$event'])
  beforeinstallprompt(event: any) {
    event.preventDefault();
  }
}
