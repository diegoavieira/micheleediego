import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { WebTheming } from '../common/web-theming/web-theming';

@Component({
  selector: 'web-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {
  theme: WebTheming;
  darkTheme: boolean;

  constructor(private router: Router) {
    const path = localStorage.getItem('PATH_KEY');

    if (path) {
      localStorage.removeItem('PATH_KEY');
      this.router.navigateByUrl(path);
    }
  }

  ngOnInit() {
    this.theme = {
      primary: '#28A1A3',
      accent: '#638665',
      warn: '#ec524b',
      darker: '#566052',
      background: '#fcfcfc'
    };

    this.darkTheme = false;
  }
}
