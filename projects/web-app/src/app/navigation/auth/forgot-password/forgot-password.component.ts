import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { WebNotificationService } from '../../../common/web-notification/web-notification.service';
import { AuthService } from '../auth.service';

@Component({
  selector: 'web-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {
  formGroup: FormGroup;
  loading: boolean;
  emailRegx: RegExp;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private webNotificationService: WebNotificationService
  ) {}

  ngOnInit() {
    this.emailRegx = /^(([^<>+()\[\]\\.,;:\s@"-#$%&=]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,3}))$/;

    this.formGroup = this.formBuilder.group({
      email: ['', [Validators.required, Validators.pattern(this.emailRegx)]]
    });
  }

  onSubmit() {
    this.loading = true;

    const { email } = {
      email: this.formGroup.value.email
    };

    this.authService.forgotPassword(email).subscribe(
      () => {
        this.loading = false;
        this.webNotificationService.open({
          message: 'Confira seu e-mail para redefinir a senha.',
          label: 'OK',
          notAutoClose: true
        });
      },
      (error) => {
        this.loading = false;

        if (error.code === 'auth/user-not-found') {
          this.formGroup.controls['email'].setErrors({ invalid: true });
        } else {
          this.webNotificationService.open({ message: 'Aconteceu um erro inesperado. Tente novamente mais tarde.' });
        }
      }
    );
  }
}
