import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegisterRoutingModule } from './register-routing.module';
import { RegisterComponent } from './register.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { WebNotificationModule } from '../../../common/web-notification/web-notification.module';
import { MatIconModule } from '@angular/material/icon';
import { WebBoxModule } from '../../../common/web-box/web-box.module';
import { WebButtonModule } from '../../../common/web-button/web-button.module';
import { WebCardModule } from '../../../common/web-card/web-card.module';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  declarations: [RegisterComponent],
  imports: [
    CommonModule,
    RegisterRoutingModule,
    FlexLayoutModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatButtonModule,
    WebButtonModule,
    MatInputModule,
    WebBoxModule,
    MatIconModule,
    WebCardModule,
    WebNotificationModule
  ]
})
export class RegisterModule {}
