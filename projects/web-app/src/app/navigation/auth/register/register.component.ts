import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { WebNotificationService } from '../../../common/web-notification/web-notification.service';
import { Account } from '../../client/account/account';
import { Role } from '../../client/account/account';
import { AuthService } from '../auth.service';

@Component({
  selector: 'web-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  loading: boolean;
  redirectTo: string;
  hidePass = true;
  emailRegx: RegExp;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private webNotificationService: WebNotificationService
  ) {}

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe((params) => {
      this.redirectTo = params['redirectTo'] || '/';
    });

    this.emailRegx = /^(([^<>+()\[\]\\.,;:\s@"-#$%&=]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,3}))$/;

    this.registerForm = this.formBuilder.group({
      displayName: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.pattern(this.emailRegx)]],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
  }

  onSubmit() {
    this.loading = true;

    const newAccount: Account = {
      displayName: this.registerForm.value.displayName,
      email: this.registerForm.value.email,
      password: this.registerForm.value.password,
      role: Role.Client
    };

    this.authService.register(newAccount).subscribe(
      () => {
        this.router.navigate([this.redirectTo]);
        this.loading = false;
      },
      (error) => {
        console.log(error);
        this.loading = false;
        if (error.code === 'auth/email-already-in-use') {
          this.registerForm.controls['email'].setErrors({ invalid: true });
        } else {
          this.webNotificationService.open({ message: 'Aconteceu um erro inesperado. Tente novamente mais tarde.' });
        }
      }
    );
  }
}
