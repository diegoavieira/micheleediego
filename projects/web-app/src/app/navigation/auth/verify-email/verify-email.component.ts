import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { WebNotificationService } from '../../../common/web-notification/web-notification.service';
import { AuthService } from '../auth.service';

@Component({
  selector: 'web-verify-email',
  templateUrl: './verify-email.component.html',
  styleUrls: ['./verify-email.component.scss']
})
export class VerifyEmailComponent implements OnInit {
  formGroup: FormGroup;
  loading: boolean;
  code: string;
  hidePass = true;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private webNotificationService: WebNotificationService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe((params) => {
      this.code = params['oobCode'] || '';
    });

    this.formGroup = this.formBuilder.group({
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
  }

  onSubmit() {
    this.loading = true;

    const { password } = {
      password: this.formGroup.value.password
    };

    this.authService.confirmPasswordReset(this.code, password).subscribe(
      () => {
        this.loading = false;
        this.router.navigate(['/auth/login']);
      },
      (error) => {
        this.loading = false;

        if (error.code === 'auth/invalid-action-code') {
          this.webNotificationService.open({ message: 'Link expirado. Solicite novamente Redefinir Senha.' });
        } else {
          this.webNotificationService.open({ message: 'Aconteceu um erro inesperado. Tente novamente mais tarde.' });
        }
      }
    );
  }
}
