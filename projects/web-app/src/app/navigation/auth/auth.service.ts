import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { from, Observable, of, throwError } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import { Account, Role } from '../client/account/account';
import { AccountService } from '../client/account/account.service';

@Injectable({ providedIn: 'root' })
export class AuthService {
  constructor(
    private accountService: AccountService,
    private angularFireAuth: AngularFireAuth,
    private router: Router
  ) {}

  register(account: Account): Observable<boolean> {
    return from(this.angularFireAuth.createUserWithEmailAndPassword(account.email, account.password)).pipe(
      switchMap(async (userCredential) => {
        await this.accountService.create({ ...account, uid: userCredential.user.uid });
        return true;
      }),
      catchError((error) => throwError(error))
    );
  }

  login(email: string, password: string): Observable<boolean> {
    return from(this.angularFireAuth.signInWithEmailAndPassword(email, password)).pipe(
      map(() => true),
      catchError((error) => throwError(error))
    );
  }

  forgotPassword(email: string): Observable<boolean> {
    return from(this.angularFireAuth.sendPasswordResetEmail(email)).pipe(
      map(() => true),
      catchError((error) => throwError(error))
    );
  }

  confirmPasswordReset(code: string, email: string): Observable<boolean> {
    return from(this.angularFireAuth.confirmPasswordReset(code, email)).pipe(
      map(() => true),
      catchError((error) => throwError(error))
    );
  }

  accountAuth(): Observable<Account> {
    return this.angularFireAuth.authState.pipe(
      switchMap((userCredential) => (userCredential ? this.accountService.getByUid(userCredential.uid) : of(null))),
      catchError((error) => throwError(error))
    );
  }

  authenticated(): Observable<boolean> {
    return this.angularFireAuth.authState.pipe(
      map((userCredential) => (userCredential ? true : false)),
      catchError((error) => throwError(error))
    );
  }

  isRole(role: Role): Observable<boolean> {
    return this.accountAuth().pipe(
      map((account) => (account && account.role.includes(role) ? true : false)),
      catchError((error) => throwError(error))
    );
  }

  logout() {
    this.angularFireAuth.signOut();
    this.router.navigate(['/']);
  }
}
