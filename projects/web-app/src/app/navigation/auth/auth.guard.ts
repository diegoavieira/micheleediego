import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { AuthService } from './auth.service';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
  constructor(private authService: AuthService, private router: Router) {}

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.authService.accountAuth().pipe(
      take(1),
      map((account) => {
        if (account) {
          if (next.data.roles && next.data.roles.indexOf(account.role) === -1) {
            this.router.navigate(['/auth/login'], { queryParams: { redirectTo: state.url } });
            return false;
          }

          return true;
        }

        this.router.navigate(['/auth/login'], { queryParams: { redirectTo: state.url } });
        return false;
      })
    );
  }
}
