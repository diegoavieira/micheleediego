import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthRoutingModule } from './auth-routing.module';
import { AuthComponent } from './auth.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { WebMainModule } from '../../common/web-main/web-main.module';
import { WebSidenavModule } from '../../common/web-sidenav/web-sidenav.module';
import { WebFooterModule } from '../../common/web-footer/web-footer.module';
import { WebSvgModule } from '../../common/web-svg/web-svg.module';

@NgModule({
  declarations: [AuthComponent],
  imports: [
    CommonModule,
    AuthRoutingModule,
    FlexLayoutModule,
    WebMainModule,
    WebSidenavModule,
    WebFooterModule,
    WebSvgModule
  ]
})
export class AuthModule {}
