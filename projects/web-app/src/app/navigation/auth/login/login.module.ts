import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { WebBoxModule } from '../../../common/web-box/web-box.module';
import { MatIconModule } from '@angular/material/icon';
import { WebCardModule } from '../../../common/web-card/web-card.module';
import { WebButtonModule } from '../../../common/web-button/web-button.module';
import { WebNotificationModule } from '../../../common/web-notification/web-notification.module';

@NgModule({
  declarations: [LoginComponent],
  imports: [
    CommonModule,
    LoginRoutingModule,
    FlexLayoutModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatButtonModule,
    WebButtonModule,
    MatInputModule,
    WebBoxModule,
    MatIconModule,
    WebCardModule,
    WebNotificationModule
  ]
})
export class LoginModule {}
