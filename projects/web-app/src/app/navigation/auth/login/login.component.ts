import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { WebNotificationService } from '../../../common/web-notification/web-notification.service';
import { AuthService } from '../auth.service';

@Component({
  selector: 'web-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  loading: boolean;
  redirectTo: string;
  hidePass = true;
  emailRegx: RegExp;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private webNotificationService: WebNotificationService
  ) {}

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe((params) => {
      this.redirectTo = params['redirectTo'] || '/';
    });

    this.emailRegx = /^(([^<>+()\[\]\\.,;:\s@"-#$%&=]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,3}))$/;

    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.pattern(this.emailRegx)]],
      password: ['', [Validators.required]]
    });
  }

  onSubmit() {
    this.loading = true;

    const { email, password } = {
      email: this.loginForm.value.email,
      password: this.loginForm.value.password
    };

    this.authService.login(email, password).subscribe(
      () => {
        this.router.navigate([this.redirectTo]);
        this.loading = false;
      },
      (error) => {
        this.loading = false;

        if (error.code === 'auth/wrong-password') {
          this.loginForm.controls['password'].setErrors({ invalid: true });
        } else if (error.code === 'auth/user-not-found') {
          this.loginForm.controls['email'].setErrors({ invalid: true });
        } else {
          this.webNotificationService.open({ message: 'Aconteceu um erro inesperado. Tente novamente mais tarde.' });
        }
      }
    );
  }
}
