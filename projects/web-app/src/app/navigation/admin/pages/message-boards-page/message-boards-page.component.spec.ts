import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MessageBoardsPageComponent } from './message-boards-page.component';

describe('MessageBoardsPageComponent', () => {
  let component: MessageBoardsPageComponent;
  let fixture: ComponentFixture<MessageBoardsPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MessageBoardsPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessageBoardsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
