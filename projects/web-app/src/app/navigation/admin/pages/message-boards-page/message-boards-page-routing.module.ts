import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MessageBoardsPageComponent } from './message-boards-page.component';

const routes: Routes = [
  {
    path: '',
    component: MessageBoardsPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MessageBoardsPageRoutingModule {}
