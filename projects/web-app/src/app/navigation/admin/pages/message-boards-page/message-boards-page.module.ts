import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MessageBoardsPageRoutingModule } from './message-boards-page-routing.module';
import { MessageBoardsPageComponent } from './message-boards-page.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { WebButtonModule } from 'projects/web-app/src/app/common/web-button/web-button.module';
import { WebNotificationModule } from 'projects/web-app/src/app/common/web-notification/web-notification.module';

@NgModule({
  declarations: [MessageBoardsPageComponent],
  imports: [
    CommonModule,
    MessageBoardsPageRoutingModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    WebButtonModule,
    FlexLayoutModule,
    MatInputModule,
    WebNotificationModule
  ]
})
export class MessageBoardsPageModule {}
