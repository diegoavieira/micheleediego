export interface OurStoryPage {
  mainTitleImageUrl?: string;
  mainTitle: string;
  contentTextA?: string;
  contentImageUrlA?: string;
}
