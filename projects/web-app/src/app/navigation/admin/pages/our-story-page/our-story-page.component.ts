import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { WebNotificationService } from 'projects/web-app/src/app/common/web-notification/web-notification.service';
import { Page } from '../pages';
import { PagesService } from '../pages.service';

@Component({
  selector: 'web-our-story-page',
  templateUrl: './our-story-page.component.html',
  styleUrls: ['./our-story-page.component.scss']
})
export class OurStoryPageComponent implements OnInit {
  formGroup: FormGroup;
  loading: boolean;
  pageName = 'our-story';
  page: Page;

  constructor(
    private formBuilder: FormBuilder,
    private pagesService: PagesService,
    private webNotificationService: WebNotificationService
  ) {}

  ngOnInit() {
    this.formGroup = this.formBuilder.group({
      mainTitle: ['', [Validators.required]],
      mainTitleImageUrl: [''],
      contentImageUrlA: [''],
      contentTextA: ['']
    });

    this.setFormValues();
  }

  setFormValues() {
    this.pagesService.getByName(this.pageName).subscribe((page) => {
      if (page) {
        this.page = page;

        this.page.values.contentTextA = this.page.values.contentTextA.join('\n');

        this.formGroup.setValue(this.page.values);
      } else {
        this.page = { name: this.pageName, values: {} };
      }
    });
  }

  onSubmit() {
    this.loading = true;

    this.page.values = {
      mainTitle: this.formGroup.value.mainTitle,
      mainTitleImageUrl: this.formGroup.value.mainTitleImageUrl,
      contentImageUrlA: this.formGroup.value.contentImageUrlA,
      contentTextA: this.formGroup.value.contentTextA.split('\n')
    };

    this.pagesService.create(this.page).then(
      () => {
        this.loading = false;
        this.setFormValues();
        this.webNotificationService.open({ message: 'Dados alterados com sucesso!' });
      },
      () => {
        this.loading = false;
        this.webNotificationService.open({ message: 'Aconteceu um erro inesperado. Tente novamente mais tarde.' });
      }
    );
  }

  onReset() {
    this.setFormValues();
  }
}
