import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OurStoryPageComponent } from './our-story-page.component';

const routes: Routes = [
  {
    path: '',
    component: OurStoryPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OurStoryPageRoutingModule {}
