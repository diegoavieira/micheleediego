export interface Page {
  uid?: string;
  name: string;
  values: any;
}
