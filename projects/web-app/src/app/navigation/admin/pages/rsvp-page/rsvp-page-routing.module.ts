import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RsvpPageComponent } from './rsvp-page.component';

const routes: Routes = [
  {
    path: '',
    component: RsvpPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RsvpPageRoutingModule {}
