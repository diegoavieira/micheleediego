import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PagesComponent } from './pages.component';

const routes: Routes = [
  {
    path: '',
    component: PagesComponent,
    children: [
      { path: '', redirectTo: 'home', pathMatch: 'full' },
      {
        path: 'home',
        loadChildren: () => import('./home-page/home-page.module').then((m) => m.HomePageModule)
      },
      {
        path: 'our-story',
        loadChildren: () => import('./our-story-page/our-story-page.module').then((m) => m.OurStoryPageModule)
      },
      {
        path: 'rsvp',
        loadChildren: () => import('./rsvp-page/rsvp-page.module').then((m) => m.RsvpPageModule)
      },
      {
        path: 'gifts-list',
        loadChildren: () => import('./gifts-list-page/gifts-list-page.module').then((m) => m.GiftsListPageModule)
      },
      {
        path: 'message-boards',
        loadChildren: () =>
          import('./message-boards-page/message-boards-page.module').then((m) => m.MessageBoardsPageModule)
      },
      {
        path: 'location',
        loadChildren: () => import('./location-page/location-page.module').then((m) => m.LocationPageModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule {}
