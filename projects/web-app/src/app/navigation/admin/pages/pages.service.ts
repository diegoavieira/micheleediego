import { Injectable } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Page } from './pages';

@Injectable({ providedIn: 'root' })
export class PagesService {
  private pageCollection: AngularFirestoreCollection<Page>;

  constructor(private angularFirestore: AngularFirestore) {
    this.pageCollection = this.angularFirestore.collection('page');
  }

  getAll(): Observable<Page[]> {
    return this.pageCollection.valueChanges();
  }

  getByUid(uid: string): Observable<Page> {
    return this.pageCollection.doc<Page>(uid).valueChanges();
  }

  getByName(name: string): Observable<Page> {
    return this.angularFirestore
      .collection<Page>('page', (ref) => ref.where('name', '==', name))
      .valueChanges()
      .pipe(map((value) => value[0]));
  }

  create(page: Page): Promise<void> {
    if (!page.uid) {
      page.uid = this.angularFirestore.createId();
    }

    return this.pageCollection.doc(page.uid).set(page, { merge: true });
  }

  delete(page: Page): Promise<void> {
    return this.pageCollection.doc<Page>(page.uid).delete();
  }
}
