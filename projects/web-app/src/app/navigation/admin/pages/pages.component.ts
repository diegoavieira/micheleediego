import { Component, OnInit } from '@angular/core';
import { WebTabs } from '../../../common/web-tabs/web-tabs';

@Component({
  selector: 'web-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.scss']
})
export class PagesComponent implements OnInit {
  webTabs: WebTabs;

  constructor() {}

  ngOnInit() {
    this.webTabs = {
      tabs: [
        {
          title: 'Principal',
          url: 'home'
        },
        {
          title: 'Nossa História',
          url: 'our-story'
        },
        {
          title: 'Confirmar Presença',
          url: 'rsvp'
        },
        {
          title: 'Lista de Presentes',
          url: 'gifts-list'
        },
        {
          title: 'Mural de Recados',
          url: 'message-boards'
        },
        {
          title: 'Localização',
          url: 'location'
        }
      ]
    };
  }
}
