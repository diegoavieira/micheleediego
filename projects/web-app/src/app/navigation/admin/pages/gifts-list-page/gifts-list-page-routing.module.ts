import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GiftsListPageComponent } from './gifts-list-page.component';

const routes: Routes = [
  {
    path: '',
    component: GiftsListPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GiftsListPageRoutingModule {}
