import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GiftsListPageComponent } from './gifts-list-page.component';

describe('GiftsListPageComponent', () => {
  let component: GiftsListPageComponent;
  let fixture: ComponentFixture<GiftsListPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GiftsListPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GiftsListPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
