export interface HomePage {
  featuredImageUrl?: string;
  featuredPhrase?: string;
  featuredSubPhrase?: string;
  featuredMonogram?: string;
  featuredPhraseMonogram?: string;
  greetingsTitle?: string;
  greetingsTitleImageUrl?: string;
  greetingsImageUrl?: string;
  greetingsText?: string;
}
