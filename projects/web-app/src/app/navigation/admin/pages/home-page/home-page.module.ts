import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomePageRoutingModule } from './home-page-routing.module';
import { HomePageComponent } from './home-page.component';
import { WebButtonModule } from '../../../../common/web-button/web-button.module';
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatInputModule } from '@angular/material/input';
import { WebNotificationModule } from '../../../../common/web-notification/web-notification.module';

@NgModule({
  declarations: [HomePageComponent],
  imports: [
    CommonModule,
    HomePageRoutingModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    WebButtonModule,
    FlexLayoutModule,
    MatInputModule,
    WebNotificationModule
  ]
})
export class HomePageModule {}
