import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { WebNotificationService } from 'projects/web-app/src/app/common/web-notification/web-notification.service';
import { WebButton } from '../../../../common/web-button/web-button';
import { Page } from '../pages';
import { PagesService } from '../pages.service';

@Component({
  selector: 'web-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {
  formGroup: FormGroup;
  loading: boolean;
  pageName = 'home';
  page: Page;

  constructor(
    private formBuilder: FormBuilder,
    private pagesService: PagesService,
    private webNotificationService: WebNotificationService
  ) {}

  ngOnInit() {
    this.formGroup = this.formBuilder.group({
      featuredImageUrl: ['', [Validators.required]],
      featuredPhrase: [''],
      featuredSubPhrase: [''],
      featuredMonogram: [''],
      featuredPhraseMonogram: [''],
      greetingsTitle: [''],
      greetingsTitleImageUrl: [''],
      greetingsImageUrl: [''],
      greetingsText: ['']
    });

    this.setFormValues();
  }

  setFormValues() {
    this.pagesService.getByName(this.pageName).subscribe((page) => {
      if (page) {
        this.page = page;

        this.page.values.greetingsText = this.page.values.greetingsText.join('\n');

        this.formGroup.setValue(this.page.values);
      } else {
        this.page = { name: this.pageName, values: {} };
      }
    });
  }

  onSubmit() {
    this.loading = true;

    this.page.values = {
      featuredImageUrl: this.formGroup.value.featuredImageUrl,
      featuredPhrase: this.formGroup.value.featuredPhrase,
      featuredSubPhrase: this.formGroup.value.featuredSubPhrase,
      featuredMonogram: this.formGroup.value.featuredMonogram,
      featuredPhraseMonogram: this.formGroup.value.featuredPhraseMonogram,
      greetingsTitle: this.formGroup.value.greetingsTitle,
      greetingsTitleImageUrl: this.formGroup.value.greetingsTitleImageUrl,
      greetingsImageUrl: this.formGroup.value.greetingsImageUrl,
      greetingsText: this.formGroup.value.greetingsText.split('\n')
    };

    this.pagesService.create(this.page).then(
      () => {
        this.loading = false;
        this.setFormValues();
        this.webNotificationService.open({ message: 'Dados alterados com sucesso!' });
      },
      () => {
        this.loading = false;
        this.webNotificationService.open({ message: 'Aconteceu um erro inesperado. Tente novamente mais tarde.' });
      }
    );
  }

  onReset() {
    this.setFormValues();
  }
}
