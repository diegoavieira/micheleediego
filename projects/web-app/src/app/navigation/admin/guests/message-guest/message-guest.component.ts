import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { WebNotificationService } from 'projects/web-app/src/app/common/web-notification/web-notification.service';
import { Observable } from 'rxjs';
import { MessageGuest } from './message-guest';
import { MessageGuestService } from './message-guest.service';

@Component({
  selector: 'web-message-guest',
  templateUrl: './message-guest.component.html',
  styleUrls: ['./message-guest.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class MessageGuestComponent implements OnInit {
  messageGuests$: Observable<MessageGuest[]>;
  loading: boolean;

  constructor(
    private messageGuestService: MessageGuestService,
    private webNotificationService: WebNotificationService
  ) {}

  ngOnInit() {
    this.messageGuests$ = this.messageGuestService.getAllByOrderDate();
  }

  onChangeStatus(event: MatCheckboxChange, messageGuest: MessageGuest) {
    this.loading = true;

    messageGuest.checked = !messageGuest.checked;

    this.messageGuestService.create(messageGuest).then(
      () => {
        this.loading = false;
        this.webNotificationService.open({ message: 'Status alterado com sucesso com sucesso!' });
      },
      () => {
        this.loading = false;
        this.webNotificationService.open({ message: 'Aconteceu um erro inesperado. Tente novamente mais tarde.' });
      }
    );
  }
}
