export interface MessageGuest {
  uid?: string;
  name: string;
  comment?: string;
  date: Date;
  checked: boolean;
}
