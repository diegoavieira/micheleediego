import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MessageGuestRoutingModule } from './message-guest-routing.module';
import { MessageGuestComponent } from './message-guest.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { WebCardModule } from 'projects/web-app/src/app/common/web-card/web-card.module';
import { WebLoadingModule } from 'projects/web-app/src/app/common/web-loading/web-loading.module';
import { WebNotificationModule } from 'projects/web-app/src/app/common/web-notification/web-notification.module';

@NgModule({
  declarations: [MessageGuestComponent],
  imports: [
    CommonModule,
    MessageGuestRoutingModule,
    MatCheckboxModule,
    WebCardModule,
    FlexLayoutModule,
    WebNotificationModule,
    WebLoadingModule
  ]
})
export class MessageGuestModule {}
