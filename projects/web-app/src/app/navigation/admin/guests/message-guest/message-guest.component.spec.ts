import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MessageGuestComponent } from './message-guest.component';

describe('MessageGuestComponent', () => {
  let component: MessageGuestComponent;
  let fixture: ComponentFixture<MessageGuestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MessageGuestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessageGuestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
