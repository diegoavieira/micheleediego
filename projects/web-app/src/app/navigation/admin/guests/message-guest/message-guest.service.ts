import { Injectable } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { MessageGuest } from './message-guest';

@Injectable({ providedIn: 'root' })
export class MessageGuestService {
  private messageGuestCollection: AngularFirestoreCollection<MessageGuest>;

  constructor(private angularFirestore: AngularFirestore) {
    this.messageGuestCollection = this.angularFirestore.collection('message-guest');
  }

  getAll(): Observable<MessageGuest[]> {
    return this.messageGuestCollection.valueChanges();
  }

  getByUid(uid: string): Observable<MessageGuest> {
    return this.messageGuestCollection.doc<MessageGuest>(uid).valueChanges();
  }

  getAllByChecked(checked: boolean): Observable<MessageGuest[]> {
    return this.angularFirestore
      .collection<MessageGuest>('message-guest', (ref) => ref.where('checked', '==', checked))
      .valueChanges();
  }

  getAllByOrderDate(): Observable<MessageGuest[]> {
    return this.angularFirestore
      .collection<MessageGuest>('message-guest', (ref) => ref.orderBy('date', 'desc'))
      .valueChanges();
  }

  create(rsvpGuest: MessageGuest): Promise<void> {
    if (!rsvpGuest.uid) {
      rsvpGuest.uid = this.angularFirestore.createId();
    }

    return this.messageGuestCollection.doc<MessageGuest>(rsvpGuest.uid).set(rsvpGuest, { merge: true });
  }

  delete(rsvpGuest: MessageGuest): Promise<void> {
    return this.messageGuestCollection.doc<MessageGuest>(rsvpGuest.uid).delete();
  }
}
