import { TestBed } from '@angular/core/testing';

import { MessageGuestService } from './message-guest.service';

describe('MessageGuestService', () => {
  let service: MessageGuestService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MessageGuestService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
