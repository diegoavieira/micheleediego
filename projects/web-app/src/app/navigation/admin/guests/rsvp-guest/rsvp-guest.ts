export interface RsvpGuest {
  uid?: string;
  name: string;
  phone: string;
  guestsName: string;
  comment?: string;
  date: string;
  checked: boolean;
}
