import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RsvpGuestComponent } from './rsvp-guest.component';

describe('RsvpGuestComponent', () => {
  let component: RsvpGuestComponent;
  let fixture: ComponentFixture<RsvpGuestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RsvpGuestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RsvpGuestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
