import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RsvpGuestRoutingModule } from './rsvp-guest-routing.module';
import { RsvpGuestComponent } from './rsvp-guest.component';
import { WebCardModule } from 'projects/web-app/src/app/common/web-card/web-card.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { WebNotificationModule } from 'projects/web-app/src/app/common/web-notification/web-notification.module';
import { WebLoadingModule } from 'projects/web-app/src/app/common/web-loading/web-loading.module';

@NgModule({
  declarations: [RsvpGuestComponent],
  imports: [
    CommonModule,
    RsvpGuestRoutingModule,
    MatCheckboxModule,
    WebCardModule,
    FlexLayoutModule,
    WebNotificationModule,
    WebLoadingModule
  ]
})
export class RsvpGuestModule {}
