import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RsvpGuestComponent } from './rsvp-guest.component';

const routes: Routes = [
  {
    path: '',
    component: RsvpGuestComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RsvpGuestRoutingModule {}
