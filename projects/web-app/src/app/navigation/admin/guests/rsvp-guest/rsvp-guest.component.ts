import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { WebNotificationService } from 'projects/web-app/src/app/common/web-notification/web-notification.service';
import { Observable } from 'rxjs';
import { RsvpGuest } from './rsvp-guest';
import { RsvpGuestService } from './rsvp-guest.service';

@Component({
  selector: 'web-rsvp-guest',
  templateUrl: './rsvp-guest.component.html',
  styleUrls: ['./rsvp-guest.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class RsvpGuestComponent implements OnInit {
  rsvpGuests$: Observable<RsvpGuest[]>;
  loading: boolean;

  constructor(private rsvpGuestService: RsvpGuestService, private webNotificationService: WebNotificationService) {}

  ngOnInit() {
    this.rsvpGuests$ = this.rsvpGuestService.getAllByOrderDate();
  }

  onChangeStatus(event: MatCheckboxChange, rspvGuest: RsvpGuest) {
    this.loading = true;

    rspvGuest.checked = !rspvGuest.checked;

    this.rsvpGuestService.create(rspvGuest).then(
      () => {
        this.loading = false;
        this.webNotificationService.open({ message: 'Status alterado com sucesso com sucesso!' });
      },
      () => {
        this.loading = false;
        this.webNotificationService.open({ message: 'Aconteceu um erro inesperado. Tente novamente mais tarde.' });
      }
    );
  }
}
