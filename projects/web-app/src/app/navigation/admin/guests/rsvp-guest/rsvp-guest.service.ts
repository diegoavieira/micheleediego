import { Injectable } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { RsvpGuest } from './rsvp-guest';

@Injectable({ providedIn: 'root' })
export class RsvpGuestService {
  private rsvpGuestCollection: AngularFirestoreCollection<RsvpGuest>;

  constructor(private angularFirestore: AngularFirestore) {
    this.rsvpGuestCollection = this.angularFirestore.collection('rsvp-guest');
  }

  getAll(): Observable<RsvpGuest[]> {
    return this.rsvpGuestCollection.valueChanges();
  }

  getByUid(uid: string): Observable<RsvpGuest> {
    return this.rsvpGuestCollection.doc<RsvpGuest>(uid).valueChanges();
  }

  getAllByChecked(checked: boolean): Observable<RsvpGuest[]> {
    return this.angularFirestore
      .collection<RsvpGuest>('rsvp-guest', (ref) => ref.where('checked', '==', checked))
      .valueChanges();
  }

  getAllByOrderDate(): Observable<RsvpGuest[]> {
    return this.angularFirestore
      .collection<RsvpGuest>('rsvp-guest', (ref) => ref.orderBy('date', 'desc'))
      .valueChanges();
  }

  create(rsvpGuest: RsvpGuest): Promise<void> {
    if (!rsvpGuest.uid) {
      rsvpGuest.uid = this.angularFirestore.createId();
    }

    return this.rsvpGuestCollection.doc<RsvpGuest>(rsvpGuest.uid).set(rsvpGuest, { merge: true });
  }

  delete(rsvpGuest: RsvpGuest): Promise<void> {
    return this.rsvpGuestCollection.doc<RsvpGuest>(rsvpGuest.uid).delete();
  }
}
