import { TestBed } from '@angular/core/testing';

import { RsvpGuestService } from './rsvp-guest.service';

describe('RsvpGuestService', () => {
  let service: RsvpGuestService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RsvpGuestService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
