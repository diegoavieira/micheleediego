export interface Guest {
  uid?: string;
  name: string;
  city?: string;
  phone?: string;
  email?: string;
}
