import { Component, OnInit } from '@angular/core';
import { WebTabs } from '../../../common/web-tabs/web-tabs';

@Component({
  selector: 'web-guests',
  templateUrl: './guests.component.html',
  styleUrls: ['./guests.component.scss']
})
export class GuestsComponent implements OnInit {
  webTabs: WebTabs;

  constructor() {}

  ngOnInit() {
    this.webTabs = {
      tabs: [
        {
          title: 'Confirmações',
          url: 'rsvp'
        },
        {
          title: 'Presentes',
          url: 'gifts'
        },
        {
          title: 'Recados',
          url: 'message'
        }
      ]
    };
  }
}
