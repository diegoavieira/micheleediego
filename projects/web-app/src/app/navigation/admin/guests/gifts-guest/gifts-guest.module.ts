import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GiftsGuestRoutingModule } from './gifts-guest-routing.module';
import { GiftsGuestComponent } from './gifts-guest.component';
import { WebLoadingModule } from 'projects/web-app/src/app/common/web-loading/web-loading.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { WebCardModule } from 'projects/web-app/src/app/common/web-card/web-card.module';
import { WebNotificationModule } from 'projects/web-app/src/app/common/web-notification/web-notification.module';

@NgModule({
  declarations: [GiftsGuestComponent],
  imports: [
    CommonModule,
    GiftsGuestRoutingModule,
    MatCheckboxModule,
    WebCardModule,
    FlexLayoutModule,
    WebNotificationModule,
    WebLoadingModule
  ]
})
export class GiftsGuestModule {}
