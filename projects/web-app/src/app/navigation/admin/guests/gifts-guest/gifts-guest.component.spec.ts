import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GiftsGuestComponent } from './gifts-guest.component';

describe('GiftsGuestComponent', () => {
  let component: GiftsGuestComponent;
  let fixture: ComponentFixture<GiftsGuestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GiftsGuestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GiftsGuestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
