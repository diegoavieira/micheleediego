import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GiftsGuestComponent } from './gifts-guest.component';

const routes: Routes = [
  {
    path: '',
    component: GiftsGuestComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GiftsGuestRoutingModule {}
