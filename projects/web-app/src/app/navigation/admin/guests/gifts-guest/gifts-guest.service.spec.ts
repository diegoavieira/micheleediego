import { TestBed } from '@angular/core/testing';

import { GiftsGuestService } from './gifts-guest.service';

describe('GiftsGuestService', () => {
  let service: GiftsGuestService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GiftsGuestService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
