import { Injectable } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { GiftsGuest } from './gifts-guest';

@Injectable({ providedIn: 'root' })
export class GiftsGuestService {
  private giftsGuestCollection: AngularFirestoreCollection<GiftsGuest>;

  constructor(private angularFirestore: AngularFirestore) {
    this.giftsGuestCollection = this.angularFirestore.collection('gifts-guest');
  }

  getAll(): Observable<GiftsGuest[]> {
    return this.giftsGuestCollection.valueChanges();
  }

  getByUid(uid: string): Observable<GiftsGuest> {
    return this.giftsGuestCollection.doc<GiftsGuest>(uid).valueChanges();
  }

  getAllByChecked(checked: boolean): Observable<GiftsGuest[]> {
    return this.angularFirestore
      .collection<GiftsGuest>('gifts-guest', (ref) => ref.where('checked', '==', checked))
      .valueChanges();
  }

  getAllByOrderDate(): Observable<GiftsGuest[]> {
    return this.angularFirestore
      .collection<GiftsGuest>('gifts-guest', (ref) => ref.orderBy('date', 'desc'))
      .valueChanges();
  }

  create(rsvpGuest: GiftsGuest): Promise<void> {
    if (!rsvpGuest.uid) {
      rsvpGuest.uid = this.angularFirestore.createId();
    }

    return this.giftsGuestCollection.doc<GiftsGuest>(rsvpGuest.uid).set(rsvpGuest, { merge: true });
  }

  delete(rsvpGuest: GiftsGuest): Promise<void> {
    return this.giftsGuestCollection.doc<GiftsGuest>(rsvpGuest.uid).delete();
  }
}
