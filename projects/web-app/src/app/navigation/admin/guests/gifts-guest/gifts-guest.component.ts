import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { WebNotificationService } from 'projects/web-app/src/app/common/web-notification/web-notification.service';
import { Observable } from 'rxjs';
import { GiftsGuest } from './gifts-guest';
import { GiftsGuestService } from './gifts-guest.service';

@Component({
  selector: 'web-gifts-guest',
  templateUrl: './gifts-guest.component.html',
  styleUrls: ['./gifts-guest.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class GiftsGuestComponent implements OnInit {
  giftsGuests$: Observable<GiftsGuest[]>;
  loading: boolean;

  constructor(private giftsGuestService: GiftsGuestService, private webNotificationService: WebNotificationService) {}

  ngOnInit() {
    this.giftsGuests$ = this.giftsGuestService.getAllByOrderDate();
  }

  onChangeStatus(event: MatCheckboxChange, giftsGuest: GiftsGuest) {
    this.loading = true;

    giftsGuest.checked = !giftsGuest.checked;

    this.giftsGuestService.create(giftsGuest).then(
      () => {
        this.loading = false;
        this.webNotificationService.open({ message: 'Status alterado com sucesso com sucesso!' });
      },
      () => {
        this.loading = false;
        this.webNotificationService.open({ message: 'Aconteceu um erro inesperado. Tente novamente mais tarde.' });
      }
    );
  }
}
