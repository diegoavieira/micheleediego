export interface GiftsGuest {
  uid?: string;
  name: string;
  gift: string;
  comment?: string;
  date: Date;
  checked: boolean;
}
