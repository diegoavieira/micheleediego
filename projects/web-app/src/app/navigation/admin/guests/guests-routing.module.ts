import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GuestsComponent } from './guests.component';

const routes: Routes = [
  {
    path: '',
    component: GuestsComponent,
    children: [
      { path: '', redirectTo: 'rsvp', pathMatch: 'full' },
      {
        path: 'rsvp',
        loadChildren: () => import('./rsvp-guest/rsvp-guest.module').then((m) => m.RsvpGuestModule)
      },
      {
        path: 'gifts',
        loadChildren: () => import('./gifts-guest/gifts-guest.module').then((m) => m.GiftsGuestModule)
      },
      {
        path: 'message',
        loadChildren: () => import('./message-guest/message-guest.module').then((m) => m.MessageGuestModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GuestsRoutingModule {}
