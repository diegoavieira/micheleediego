import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { Guest } from './guests';

@Injectable({ providedIn: 'root' })
export class GuestsService {
  private guestCollection: AngularFirestoreCollection<Guest>;

  constructor(private angularFirestore: AngularFirestore) {
    this.guestCollection = this.angularFirestore.collection('guest');
  }

  getAll(): Observable<Guest[]> {
    return this.guestCollection.valueChanges();
  }

  getByUid(uid: string): Observable<Guest> {
    return this.guestCollection.doc<Guest>(uid).valueChanges();
  }

  create(guest: Guest): Promise<void> {
    if (!guest.uid) {
      guest.uid = this.angularFirestore.createId();
    }

    return this.guestCollection.doc(guest.uid).set(guest, { merge: true });
  }

  delete(guest: Guest): Promise<void> {
    return this.guestCollection.doc<Guest>(guest.uid).delete();
  }
}
