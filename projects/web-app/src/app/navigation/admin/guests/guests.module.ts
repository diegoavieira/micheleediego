import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GuestsRoutingModule } from './guests-routing.module';
import { GuestsComponent } from './guests.component';
import { WebBoxModule } from '../../../common/web-box/web-box.module';
import { WebCardModule } from '../../../common/web-card/web-card.module';
import { WebTabsModule } from '../../../common/web-tabs/web-tabs.module';

@NgModule({
  declarations: [GuestsComponent],
  imports: [CommonModule, GuestsRoutingModule, WebBoxModule, WebCardModule, WebTabsModule]
})
export class GuestsModule {}
