import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Observable } from 'rxjs';
import { GiftsGuest } from '../guests/gifts-guest/gifts-guest';
import { GiftsGuestService } from '../guests/gifts-guest/gifts-guest.service';
import { MessageGuest } from '../guests/message-guest/message-guest';
import { MessageGuestService } from '../guests/message-guest/message-guest.service';
import { RsvpGuest } from '../guests/rsvp-guest/rsvp-guest';
import { RsvpGuestService } from '../guests/rsvp-guest/rsvp-guest.service';

@Component({
  selector: 'web-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DashboardComponent implements OnInit {
  giftsGuests$: Observable<GiftsGuest[]>;
  rsvpGuests$: Observable<RsvpGuest[]>;
  messageGuests$: Observable<MessageGuest[]>;

  constructor(
    private giftsGuestService: GiftsGuestService,
    private rsvpGuestService: RsvpGuestService,
    private messageGuestService: MessageGuestService
  ) {}

  ngOnInit() {
    this.giftsGuests$ = this.giftsGuestService.getAllByChecked(false);
    this.rsvpGuests$ = this.rsvpGuestService.getAllByChecked(false);
    this.messageGuests$ = this.messageGuestService.getAllByChecked(false);
  }
}
