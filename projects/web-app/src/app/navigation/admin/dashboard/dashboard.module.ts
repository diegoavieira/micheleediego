import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { WebBoxModule } from '../../../common/web-box/web-box.module';
import { WebCardModule } from '../../../common/web-card/web-card.module';

@NgModule({
  declarations: [DashboardComponent],
  imports: [CommonModule, DashboardRoutingModule, FlexLayoutModule, WebBoxModule, WebCardModule]
})
export class DashboardModule {}
