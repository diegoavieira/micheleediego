import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';
import { WebMainModule } from '../../common/web-main/web-main.module';
import { WebSidenavModule } from '../../common/web-sidenav/web-sidenav.module';
import { WebToolbarModule } from '../../common/web-toolbar/web-toolbar.module';
import { WebFooterModule } from '../../common/web-footer/web-footer.module';
import { WebSvgModule } from '../../common/web-svg/web-svg.module';
import { WebNavModule } from '../../common/web-nav/web-nav.module';
import { WebMenuModule } from '../../common/web-menu/web-menu.module';
import { WebAvatarModule } from '../../common/web-avatar/web-avatar.module';

@NgModule({
  declarations: [AdminComponent],
  imports: [
    CommonModule,
    AdminRoutingModule,
    WebMainModule,
    WebToolbarModule,
    WebSidenavModule,
    WebFooterModule,
    WebSvgModule,
    WebNavModule,
    WebMenuModule,
    WebAvatarModule
  ]
})
export class AdminModule {}
