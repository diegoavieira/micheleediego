import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SettingsRoutingModule } from './settings-routing.module';
import { SettingsComponent } from './settings.component';
import { WebBoxModule } from '../../../common/web-box/web-box.module';
import { WebCardModule } from '../../../common/web-card/web-card.module';

@NgModule({
  declarations: [SettingsComponent],
  imports: [CommonModule, SettingsRoutingModule, WebBoxModule, WebCardModule]
})
export class SettingsModule {}
