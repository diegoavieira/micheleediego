import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { WebBox } from '../../../common/web-box/web-box';
import { WebCard } from '../../../common/web-card/web-card';

@Component({
  selector: 'web-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SettingsComponent implements OnInit {
  settingsBox: WebBox;
  settingsCard: WebCard;

  constructor() {}

  ngOnInit() {
    this.settingsBox = {
      maxWidth: '100%'
    };

    this.settingsCard = {
      title: 'Definições',
      headerAlign: 'left'
    };
  }
}
