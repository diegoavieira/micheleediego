import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './auth/auth.guard';
import { Role } from './client/account/account';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./client/client.module').then((m) => m.ClientModule)
  },
  {
    path: 'admin',
    canActivate: [AuthGuard],
    data: {
      roles: [Role.Admin]
    },
    loadChildren: () => import('./admin/admin.module').then((m) => m.AdminModule)
  },
  {
    path: 'auth',
    loadChildren: () => import('./auth/auth.module').then((m) => m.AuthModule)
  },
  {
    path: '**',
    loadChildren: () => import('./not-found/not-found.module').then((m) => m.NotFoundModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NavigationRoutingModule {}
