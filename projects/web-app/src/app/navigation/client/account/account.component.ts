import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { WebNotificationService } from '../../../common/web-notification/web-notification.service';
import { AuthService } from '../../auth/auth.service';
import { Account } from './account';
import { AccountService } from './account.service';

@Component({
  selector: 'web-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AccountComponent implements OnInit {
  accountAuth$: Observable<Account>;
  account: Account;
  formGroup: FormGroup;
  loading: boolean;

  constructor(
    private authService: AuthService,
    private formBuilder: FormBuilder,
    private webNotificationService: WebNotificationService,
    private accountService: AccountService
  ) {}

  ngOnInit() {
    this.accountAuth$ = this.authService.accountAuth();

    this.formGroup = this.formBuilder.group({
      displayName: ['', [Validators.required]]
    });

    this.setFormValues();
  }

  setFormValues() {
    this.accountAuth$.subscribe((account) => {
      this.account = account;
      this.formGroup.setValue({ displayName: account.displayName });
    });
  }

  onSubmit() {
    this.loading = true;

    this.account.displayName = this.formGroup.value.displayName;

    this.accountService.create(this.account).then(
      () => {
        this.loading = false;
        this.setFormValues();
        this.webNotificationService.open({ message: 'Dados alterados com sucesso!' });
      },
      () => {
        this.loading = false;
        this.webNotificationService.open({ message: 'Aconteceu um erro inesperado. Tente novamente mais tarde.' });
      }
    );
  }

  onReset() {
    this.setFormValues();
  }
}
