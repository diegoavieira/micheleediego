import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccountRoutingModule } from './account-routing.module';
import { AccountComponent } from './account.component';
import { WebLoadingModule } from '../../../common/web-loading/web-loading.module';
import { WebBoxModule } from '../../../common/web-box/web-box.module';
import { WebTextModule } from '../../../common/web-text/web-text.module';
import { WebImageModule } from '../../../common/web-image/web-image.module';
import { WebButtonModule } from '../../../common/web-button/web-button.module';
import { WebRowModule } from '../../../common/web-row/web-row.module';
import { WebTitleModule } from '../../../common/web-title/web-title.module';
import { WebCardModule } from '../../../common/web-card/web-card.module';
import { WebAvatarModule } from '../../../common/web-avatar/web-avatar.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { WebNotificationModule } from '../../../common/web-notification/web-notification.module';
import { MatInputModule } from '@angular/material/input';

@NgModule({
  declarations: [AccountComponent],
  imports: [
    CommonModule,
    AccountRoutingModule,
    ReactiveFormsModule,
    WebLoadingModule,
    WebBoxModule,
    WebTextModule,
    WebImageModule,
    WebButtonModule,
    WebRowModule,
    WebTitleModule,
    WebCardModule,
    WebAvatarModule,
    FlexLayoutModule,
    MatFormFieldModule,
    MatInputModule,
    WebNotificationModule
  ]
})
export class AccountModule {}
