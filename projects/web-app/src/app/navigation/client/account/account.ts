export enum Role {
  Client = 'Client',
  Admin = 'Admin'
}

export interface Account {
  uid?: string;
  email: string;
  photoURL?: string;
  displayName?: string;
  password?: string;
  role: Role;
}
