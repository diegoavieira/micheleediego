import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { Account } from './account';

@Injectable({ providedIn: 'root' })
export class AccountService {
  private accountCollection: AngularFirestoreCollection<Account>;

  constructor(private angularFirestore: AngularFirestore) {
    this.accountCollection = this.angularFirestore.collection('account');
  }

  getAll(): Observable<Account[]> {
    return this.accountCollection.valueChanges();
  }

  getByUid(uid: string): Observable<Account> {
    return this.accountCollection.doc<Account>(uid).valueChanges();
  }

  create(account: Account): Promise<void> {
    if (!account.uid) {
      account.uid = this.angularFirestore.createId();
    }

    delete account.password;

    return this.accountCollection.doc<Account>(account.uid).set(account, { merge: true });
  }

  delete(account: Account): Promise<void> {
    return this.accountCollection.doc<Account>(account.uid).delete();
  }
}
