import { Component, OnInit } from '@angular/core';
import { Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { OurStoryPage } from '../../admin/pages/our-story-page/our-story-page';
import { PagesService } from '../../admin/pages/pages.service';

@Component({
  selector: 'web-our-story',
  templateUrl: './our-story.component.html',
  styleUrls: ['./our-story.component.scss']
})
export class OurStoryComponent implements OnInit {
  ourStoryPage$: Observable<OurStoryPage>;

  constructor(private pagesService: PagesService) {}

  ngOnInit() {
    this.ourStoryPage$ = this.pagesService.getByName('our-story').pipe(switchMap((page) => of(page.values)));
  }
}
