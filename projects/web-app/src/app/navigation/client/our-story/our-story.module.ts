import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OurStoryRoutingModule } from './our-story-routing.module';
import { OurStoryComponent } from './our-story.component';
import { WebBoxModule } from '../../../common/web-box/web-box.module';
import { WebRowModule } from '../../../common/web-row/web-row.module';
import { WebImageModule } from '../../../common/web-image/web-image.module';
import { WebTitleModule } from '../../../common/web-title/web-title.module';
import { WebTextModule } from '../../../common/web-text/web-text.module';
import { WebLoadingModule } from '../../../common/web-loading/web-loading.module';

@NgModule({
  declarations: [OurStoryComponent],
  imports: [
    CommonModule,
    OurStoryRoutingModule,
    WebBoxModule,
    WebRowModule,
    WebImageModule,
    WebTitleModule,
    WebTextModule,
    WebLoadingModule
  ]
})
export class OurStoryModule {}
