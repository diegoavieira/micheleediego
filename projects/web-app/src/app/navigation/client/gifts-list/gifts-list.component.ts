import { formatDate } from '@angular/common';
import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormGroup, FormGroupDirective, FormBuilder, Validators } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { WebNotificationService } from '../../../common/web-notification/web-notification.service';
import { GiftsGuest } from '../../admin/guests/gifts-guest/gifts-guest';
import { GiftsGuestService } from '../../admin/guests/gifts-guest/gifts-guest.service';
import { GiftsListPage } from '../../admin/pages/gifts-list-page/gifts-list-page';
import { PagesService } from '../../admin/pages/pages.service';

@Component({
  selector: 'web-gifts-list',
  templateUrl: './gifts-list.component.html',
  styleUrls: ['./gifts-list.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class GiftsListComponent implements OnInit {
  giftsListPage$: Observable<GiftsListPage>;
  formGroup: FormGroup;
  loading: boolean;

  @ViewChild(FormGroupDirective) formGroupDirective: FormGroupDirective;

  constructor(
    private formBuilder: FormBuilder,
    private pagesService: PagesService,
    private giftsGuestService: GiftsGuestService,
    private webNotificationService: WebNotificationService
  ) {}

  ngOnInit() {
    this.giftsListPage$ = this.pagesService.getByName('gifts-list').pipe(switchMap((page) => of(page.values)));

    this.formGroup = this.formBuilder.group({
      name: ['', [Validators.required]],
      gift: ['', [Validators.required]],
      comment: ['']
    });
  }

  toCamicadoList() {
    window.open('https://www.camicado.com.br/lista/convidado/micheleediego', 'blank');
  }

  onSubmit() {
    this.loading = true;
    const giftsGuest: GiftsGuest = {
      name: this.formGroup.value.name,
      gift: this.formGroup.value.gift,
      comment: this.formGroup.value.comment,
      date: new Date(),
      checked: false
    };
    this.giftsGuestService.create(giftsGuest).then(
      () => {
        this.loading = false;
        this.onReset();
        this.webNotificationService.open({ message: 'Comentário enviado com sucesso!' });
      },
      () => {
        this.loading = false;
        this.webNotificationService.open({ message: 'Aconteceu um erro inesperado. Tente novamente mais tarde.' });
      }
    );
  }

  onReset() {
    this.formGroupDirective.resetForm();
  }
}
