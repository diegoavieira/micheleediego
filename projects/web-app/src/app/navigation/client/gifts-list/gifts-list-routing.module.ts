import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GiftsListComponent } from './gifts-list.component';

const routes: Routes = [
  {
    path: '',
    component: GiftsListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GiftsListRoutingModule {}
