import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GiftsListRoutingModule } from './gifts-list-routing.module';
import { GiftsListComponent } from './gifts-list.component';
import { WebBoxModule } from '../../../common/web-box/web-box.module';
import { WebImageModule } from '../../../common/web-image/web-image.module';
import { WebRowModule } from '../../../common/web-row/web-row.module';
import { WebTitleModule } from '../../../common/web-title/web-title.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { WebButtonModule } from '../../../common/web-button/web-button.module';
import { WebCardModule } from '../../../common/web-card/web-card.module';
import { WebLoadingModule } from '../../../common/web-loading/web-loading.module';
import { WebNotificationModule } from '../../../common/web-notification/web-notification.module';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  declarations: [GiftsListComponent],
  imports: [
    CommonModule,
    GiftsListRoutingModule,
    FlexLayoutModule,
    WebBoxModule,
    WebRowModule,
    WebImageModule,
    WebTitleModule,
    WebLoadingModule,
    WebCardModule,
    WebButtonModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    WebNotificationModule,
    MatButtonModule
  ]
})
export class GiftsListModule {}
