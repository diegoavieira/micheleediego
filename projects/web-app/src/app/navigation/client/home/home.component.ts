import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { MessageGuest } from '../../admin/guests/message-guest/message-guest';
import { MessageGuestService } from '../../admin/guests/message-guest/message-guest.service';
import { HomePage } from '../../admin/pages/home-page/home-page';
import { PagesService } from '../../admin/pages/pages.service';

@Component({
  selector: 'web-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class HomeComponent implements OnInit {
  homePage$: Observable<HomePage>;
  messageGuests$: Observable<MessageGuest[]>;

  constructor(private pagesService: PagesService, private messageGuestService: MessageGuestService) {}

  ngOnInit() {
    this.homePage$ = this.pagesService.getByName('home').pipe(switchMap((page) => of(page.values)));
    this.messageGuests$ = this.messageGuestService.getAllByOrderDate();
  }

  filterMessageGuests(messageGuests: MessageGuest[]): MessageGuest[] {
    return messageGuests.filter((m, i) => i < 3);
  }
}
