import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { WebBoxModule } from '../../../common/web-box/web-box.module';
import { WebImageModule } from '../../../common/web-image/web-image.module';
import { WebSvgModule } from '../../../common/web-svg/web-svg.module';
import { WebRowModule } from '../../../common/web-row/web-row.module';
import { WebTitleModule } from '../../../common/web-title/web-title.module';
import { WebButtonModule } from '../../../common/web-button/web-button.module';
import { MatDividerModule } from '@angular/material/divider';
import { WebTextModule } from '../../../common/web-text/web-text.module';
import { WebLoadingModule } from '../../../common/web-loading/web-loading.module';
import { MatIconModule } from '@angular/material/icon';
import { WebCardModule } from '../../../common/web-card/web-card.module';

@NgModule({
  declarations: [HomeComponent],
  imports: [
    CommonModule,
    HomeRoutingModule,
    FlexLayoutModule,
    WebBoxModule,
    WebImageModule,
    WebSvgModule,
    WebRowModule,
    WebTitleModule,
    WebButtonModule,
    MatDividerModule,
    WebTextModule,
    WebLoadingModule,
    MatIconModule,
    WebCardModule
  ]
})
export class HomeModule {}
