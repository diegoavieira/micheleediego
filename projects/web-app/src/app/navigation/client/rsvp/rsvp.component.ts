import { formatDate } from '@angular/common';
import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, FormGroupDirective, Validators } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { WebNotificationService } from '../../../common/web-notification/web-notification.service';
import { PagesService } from '../../admin/pages/pages.service';
import { RsvpPage } from '../../admin/pages/rsvp-page/rsvp-page';
import { RsvpGuest } from '../../admin/guests/rsvp-guest/rsvp-guest';
import { RsvpGuestService } from '../../admin/guests/rsvp-guest/rsvp-guest.service';

@Component({
  selector: 'web-rsvp',
  templateUrl: './rsvp.component.html',
  styleUrls: ['./rsvp.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class RsvpComponent implements OnInit {
  rsvpPage$: Observable<RsvpPage>;
  formGroup: FormGroup;
  loading: boolean;

  @ViewChild(FormGroupDirective) formGroupDirective: FormGroupDirective;

  constructor(
    private formBuilder: FormBuilder,
    private pagesService: PagesService,
    private rspvGuestService: RsvpGuestService,
    private webNotificationService: WebNotificationService
  ) {}

  ngOnInit() {
    this.rsvpPage$ = this.pagesService.getByName('rsvp').pipe(switchMap((page) => of(page.values)));

    this.formGroup = this.formBuilder.group({
      name: ['', [Validators.required]],
      phone: ['', [Validators.required]],
      guestsName: ['', [Validators.required]],
      comment: ['']
    });
  }

  toWhatsApp() {
    window.open(
      'https://wa.me/5548991417415?text=Olá%20Sheila,%20gostaria%20de%20confirmar%20presença%20no%20casamento%20de%20Michele%20e%20Diego',
      'blank'
    );
  }

  onSubmit() {
    this.loading = true;

    const rsvpGuest: RsvpGuest = {
      name: this.formGroup.value.name,
      phone: this.formGroup.value.phone,
      guestsName: this.formGroup.value.guestsName,
      comment: this.formGroup.value.comment,
      date: formatDate(new Date(), 'dd/MM/yyyy', 'en'),
      checked: false
    };

    this.rspvGuestService.create(rsvpGuest).then(
      () => {
        this.loading = false;
        this.onReset();
        this.webNotificationService.open({ message: 'Formulário enviado com sucesso!' });
      },
      () => {
        this.loading = false;
        this.webNotificationService.open({ message: 'Aconteceu um erro inesperado. Tente novamente mais tarde.' });
      }
    );
  }

  onReset() {
    this.formGroupDirective.resetForm();
  }
}
