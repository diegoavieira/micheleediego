import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RsvpRoutingModule } from './rsvp-routing.module';
import { RsvpComponent } from './rsvp.component';
import { WebBoxModule } from '../../../common/web-box/web-box.module';
import { WebImageModule } from '../../../common/web-image/web-image.module';
import { WebRowModule } from '../../../common/web-row/web-row.module';
import { WebTitleModule } from '../../../common/web-title/web-title.module';
import { WebLoadingModule } from '../../../common/web-loading/web-loading.module';
import { WebCardModule } from '../../../common/web-card/web-card.module';
import { WebButtonModule } from '../../../common/web-button/web-button.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { WebNotificationModule } from '../../../common/web-notification/web-notification.module';
import { MatRadioModule } from '@angular/material/radio';

@NgModule({
  declarations: [RsvpComponent],
  imports: [
    CommonModule,
    RsvpRoutingModule,
    FlexLayoutModule,
    WebBoxModule,
    WebRowModule,
    WebImageModule,
    WebTitleModule,
    WebLoadingModule,
    WebCardModule,
    WebButtonModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    WebNotificationModule,
    MatRadioModule
  ]
})
export class RsvpModule {}
