import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LocationRoutingModule } from './location-routing.module';
import { LocationComponent } from './location.component';
import { WebBoxModule } from '../../../common/web-box/web-box.module';
import { WebImageModule } from '../../../common/web-image/web-image.module';
import { WebLoadingModule } from '../../../common/web-loading/web-loading.module';
import { WebRowModule } from '../../../common/web-row/web-row.module';
import { WebTitleModule } from '../../../common/web-title/web-title.module';
import { WebCardModule } from '../../../common/web-card/web-card.module';
import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({
  declarations: [LocationComponent],
  imports: [
    CommonModule,
    LocationRoutingModule,
    WebBoxModule,
    WebRowModule,
    WebImageModule,
    WebTitleModule,
    WebLoadingModule,
    FlexLayoutModule,
    WebCardModule
  ]
})
export class LocationModule {}
