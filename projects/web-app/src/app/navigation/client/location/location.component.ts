import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { LocationPage } from '../../admin/pages/location-page/location-page';
import { PagesService } from '../../admin/pages/pages.service';

@Component({
  selector: 'web-location',
  templateUrl: './location.component.html',
  styleUrls: ['./location.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LocationComponent implements OnInit {
  locationPage$: Observable<LocationPage>;

  constructor(private pagesService: PagesService) {}

  ngOnInit() {
    this.locationPage$ = this.pagesService.getByName('location').pipe(switchMap((page) => of(page.values)));
  }

  toHotel(second?) {
    if (second) {
      window.open(
        'https://book.omnibees.com/hotelresults?c=7864&q=13424&hotel_folder=&NRooms=1&CheckIn=17122022&CheckOut=18122022&ad=2&ch=0&ag=&group_code=&Code=&loyalty_code=&lang=pt-BR&currencyId=16',
        'blank'
      );
    } else {
      window.open('http://hotelquintadabicadagua.com.br/site/', 'blank');
    }
  }

  toBooking(second?) {
    if (second) {
      window.open('http://www.booking.com/Share-wsbhzT', 'blank');
    } else {
      window.open('https://www.booking.com/Share-2PmtTq', 'blank');
    }
  }
}
