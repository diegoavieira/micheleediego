import { formatDate } from '@angular/common';
import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormGroup, FormGroupDirective, FormBuilder, Validators } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { WebNotificationService } from '../../../common/web-notification/web-notification.service';
import { MessageGuest } from '../../admin/guests/message-guest/message-guest';
import { MessageGuestService } from '../../admin/guests/message-guest/message-guest.service';
import { MessageBoardsPage } from '../../admin/pages/message-boards-page/message-boards-page';
import { PagesService } from '../../admin/pages/pages.service';

@Component({
  selector: 'web-message-boards',
  templateUrl: './message-boards.component.html',
  styleUrls: ['./message-boards.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class MessageBoardsComponent implements OnInit {
  messageBoardsPage$: Observable<MessageBoardsPage>;
  formGroup: FormGroup;
  loading: boolean;
  messageGuests$: Observable<MessageGuest[]>;

  @ViewChild(FormGroupDirective) formGroupDirective: FormGroupDirective;

  constructor(
    private formBuilder: FormBuilder,
    private pagesService: PagesService,
    private messageGuestService: MessageGuestService,
    private webNotificationService: WebNotificationService
  ) {}

  ngOnInit() {
    this.messageBoardsPage$ = this.pagesService.getByName('message-boards').pipe(switchMap((page) => of(page.values)));
    this.messageGuests$ = this.messageGuestService.getAllByOrderDate();

    this.formGroup = this.formBuilder.group({
      name: ['', [Validators.required]],
      comment: ['']
    });
  }

  onSubmit() {
    this.loading = true;

    const messageGuest: MessageGuest = {
      name: this.formGroup.value.name,
      comment: this.formGroup.value.comment,
      date: new Date(),
      checked: false
    };

    this.messageGuestService.create(messageGuest).then(
      () => {
        this.loading = false;
        this.onReset();
        this.webNotificationService.open({ message: 'Recado enviado com sucesso!' });
      },
      () => {
        this.loading = false;
        this.webNotificationService.open({ message: 'Aconteceu um erro inesperado. Tente novamente mais tarde.' });
      }
    );
  }

  onReset() {
    this.formGroupDirective.resetForm();
  }
}
