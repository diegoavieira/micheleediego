import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MessageBoardsRoutingModule } from './message-boards-routing.module';
import { MessageBoardsComponent } from './message-boards.component';
import { WebTitleModule } from '../../../common/web-title/web-title.module';
import { WebBoxModule } from '../../../common/web-box/web-box.module';
import { WebImageModule } from '../../../common/web-image/web-image.module';
import { WebRowModule } from '../../../common/web-row/web-row.module';
import { WebButtonModule } from '../../../common/web-button/web-button.module';
import { WebCardModule } from '../../../common/web-card/web-card.module';
import { WebLoadingModule } from '../../../common/web-loading/web-loading.module';
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { WebNotificationModule } from '../../../common/web-notification/web-notification.module';

@NgModule({
  declarations: [MessageBoardsComponent],
  imports: [
    CommonModule,
    MessageBoardsRoutingModule,
    FlexLayoutModule,
    WebBoxModule,
    WebRowModule,
    WebImageModule,
    WebTitleModule,
    WebLoadingModule,
    WebCardModule,
    WebButtonModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    WebNotificationModule
  ]
})
export class MessageBoardsModule {}
