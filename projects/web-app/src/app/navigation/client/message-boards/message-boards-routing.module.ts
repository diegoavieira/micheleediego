import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MessageBoardsComponent } from './message-boards.component';

const routes: Routes = [
  {
    path: '',
    component: MessageBoardsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MessageBoardsRoutingModule {}
