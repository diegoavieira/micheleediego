import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../auth/auth.guard';
import { ClientComponent } from './client.component';

const routes: Routes = [
  {
    path: '',
    component: ClientComponent,
    children: [
      { path: '', redirectTo: 'home', pathMatch: 'full' },
      {
        path: 'home',
        loadChildren: () => import('./home/home.module').then((m) => m.HomeModule)
      },
      // {
      //   path: 'our-story',
      //   loadChildren: () => import('./our-story/our-story.module').then((m) => m.OurStoryModule)
      // },
      {
        path: 'rsvp',
        loadChildren: () => import('./rsvp/rsvp.module').then((m) => m.RsvpModule)
      },
      {
        path: 'gifts-list',
        loadChildren: () => import('./gifts-list/gifts-list.module').then((m) => m.GiftsListModule)
      },
      {
        path: 'message-boards',
        loadChildren: () => import('./message-boards/message-boards.module').then((m) => m.MessageBoardsModule)
      },
      {
        path: 'location',
        loadChildren: () => import('./location/location.module').then((m) => m.LocationModule)
      },
      {
        path: 'account',
        canActivate: [AuthGuard],
        loadChildren: () => import('./account/account.module').then((m) => m.AccountModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClientRoutingModule {}
