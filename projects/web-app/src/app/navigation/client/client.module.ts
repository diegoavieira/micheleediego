import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClientRoutingModule } from './client-routing.module';
import { ClientComponent } from './client.component';
import { WebMainModule } from '../../common/web-main/web-main.module';
import { WebSidenavModule } from '../../common/web-sidenav/web-sidenav.module';
import { WebToolbarModule } from '../../common/web-toolbar/web-toolbar.module';
import { WebFooterModule } from '../../common/web-footer/web-footer.module';
import { WebSvgModule } from '../../common/web-svg/web-svg.module';
import { WebMenuModule } from '../../common/web-menu/web-menu.module';
import { WebNavModule } from '../../common/web-nav/web-nav.module';
import { WebShareModule } from '../../common/web-share/web-share.module';

@NgModule({
  declarations: [ClientComponent],
  imports: [
    CommonModule,
    ClientRoutingModule,
    WebMainModule,
    WebToolbarModule,
    WebSidenavModule,
    WebFooterModule,
    WebSvgModule,
    WebMenuModule,
    WebNavModule,
    WebShareModule
  ]
})
export class ClientModule {}
