import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotFoundRoutingModule } from './not-found-routing.module';
import { NotFoundComponent } from './not-found.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { WebFooterModule } from '../../common/web-footer/web-footer.module';
import { WebMainModule } from '../../common/web-main/web-main.module';
import { WebSidenavModule } from '../../common/web-sidenav/web-sidenav.module';
import { WebSvgModule } from '../../common/web-svg/web-svg.module';
import { WebButtonModule } from '../../common/web-button/web-button.module';
import { WebTitleModule } from '../../common/web-title/web-title.module';

@NgModule({
  declarations: [NotFoundComponent],
  imports: [
    CommonModule,
    NotFoundRoutingModule,
    FlexLayoutModule,
    WebMainModule,
    WebSidenavModule,
    WebFooterModule,
    WebSvgModule,
    WebButtonModule,
    WebTitleModule
  ]
})
export class NotFoundModule {}
