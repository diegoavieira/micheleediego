import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'web-share',
  templateUrl: './web-share.component.html',
  styleUrls: ['./web-share.component.scss']
})
export class WebShareComponent implements OnInit {
  winNavigator: any;

  constructor() {}

  ngOnInit() {
    this.winNavigator = window.navigator;
  }

  onShare() {
    if (this.winNavigator.share) {
      this.winNavigator
        .share({
          title: 'Michele & Diego',
          text: 'O amor tudo sofre, tudo crê, tudo espera, tudo suporta - I Cor 13:7',
          url: location.origin
        })
        .then(() => {
          console.log('Thanks for sharing!');
        })
        .catch(console.error);
    }
  }
}
