import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WebShareComponent } from './web-share.component';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  declarations: [WebShareComponent],
  imports: [CommonModule, MatButtonModule, MatIconModule],
  exports: [WebShareComponent]
})
export class WebShareModule {}
