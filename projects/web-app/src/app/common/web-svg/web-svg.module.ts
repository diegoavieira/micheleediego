import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WebSvgComponent } from './web-svg.component';

@NgModule({
  declarations: [WebSvgComponent],
  imports: [CommonModule],
  exports: [WebSvgComponent]
})
export class WebSvgModule {}
