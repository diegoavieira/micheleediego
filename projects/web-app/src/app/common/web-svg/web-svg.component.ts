import { AfterViewInit, Component, ElementRef, Input, OnInit, Renderer2, ViewEncapsulation } from '@angular/core';
import { WebPalette } from '../web-theming/web-theming';
import { WebSvg } from './web-svg';
@Component({
  selector: 'web-svg',
  templateUrl: './web-svg.component.html',
  styleUrls: ['./web-svg.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class WebSvgComponent implements OnInit, AfterViewInit {
  @Input() config: WebSvg;

  constructor(private renderer: Renderer2, private el: ElementRef) {}

  ngOnInit() {}

  ngAfterViewInit() {
    if (this.config.viewBox) {
      const height = `${this.config.viewBox.split(' ')[3]}px`;
      this.renderer.setProperty(this.el.nativeElement, 'style', `--web-svg-height: ${height}`);
    }

    if (this.config.color in WebPalette) {
      this.renderer.addClass(this.el.nativeElement, this.config.color);
    } else {
      this.renderer.removeClass(this.el.nativeElement, this.config.color);
    }
  }
}
