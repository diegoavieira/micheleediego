export interface WebSvg {
  viewBox: string;
  paths: string[];
  color?: string;
  margin?: string;
}
