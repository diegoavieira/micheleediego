import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WebLoadingComponent } from './web-loading.component';

describe('WebLoadingComponent', () => {
  let component: WebLoadingComponent;
  let fixture: ComponentFixture<WebLoadingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WebLoadingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WebLoadingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
