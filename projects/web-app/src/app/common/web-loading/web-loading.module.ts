import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WebLoadingComponent } from './web-loading.component';
import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({
  declarations: [WebLoadingComponent],
  imports: [CommonModule, FlexLayoutModule],
  exports: [WebLoadingComponent]
})
export class WebLoadingModule {}
