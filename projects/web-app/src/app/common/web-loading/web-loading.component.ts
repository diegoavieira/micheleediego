import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'web-loading',
  templateUrl: './web-loading.component.html',
  styleUrls: ['./web-loading.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class WebLoadingComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
