import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WebMainComponent } from './web-main.component';

@NgModule({
  declarations: [WebMainComponent],
  imports: [CommonModule],
  exports: [WebMainComponent]
})
export class WebMainModule {}
