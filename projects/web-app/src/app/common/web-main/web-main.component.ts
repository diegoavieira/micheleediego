import {
  AfterContentInit,
  Component,
  ContentChild,
  EventEmitter,
  OnInit,
  Output,
  ViewEncapsulation
} from '@angular/core';
import { BreakpointObserver } from '@angular/cdk/layout';
import { WebToolbarComponent } from '../web-toolbar/web-toolbar.component';
import { WebSidenavComponent } from '../web-sidenav/web-sidenav.component';

@Component({
  selector: 'web-main',
  templateUrl: './web-main.component.html',
  styleUrls: ['./web-main.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class WebMainComponent implements OnInit, AfterContentInit {
  isMobile: boolean;
  isAndroid: boolean;
  isPwa: boolean;

  @Output() isMobileEvent = new EventEmitter();

  @ContentChild(WebToolbarComponent) toolbar: WebToolbarComponent;

  @ContentChild(WebSidenavComponent) sidenav: WebSidenavComponent;

  constructor(public breakpointObserver: BreakpointObserver) {}

  ngOnInit() {}

  ngAfterContentInit() {
    if (!this.toolbar) {
      return;
    }

    if (!this.sidenav) {
      return;
    }

    this.toolbar.sidenavToggle.subscribe(() => {
      this.sidenav.opened = !this.sidenav.opened;
    });

    this.sidenav.sidenavToggle.subscribe(() => {
      if (this.isMobile) {
        this.sidenav.opened = !this.sidenav.opened;
      }
    });

    this.breakpointObserver.observe('(max-width: 1023px)').subscribe((bkp) => {
      this.isMobile = bkp.matches;
      this.sidenav.opened = !this.isMobile;
      this.sidenav.isMobile = this.isMobile;
      this.toolbar.isMobile = this.isMobile;
      this.isMobileEvent.emit(this.isMobile);

      this.isAndroid = /Android/i.test(navigator.userAgent) ? true : false;
      this.isPwa = window.matchMedia('(display-mode: standalone)').matches ? true : false;
    });
  }
}
