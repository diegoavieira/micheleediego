import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WebRowComponent } from './web-row.component';
import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({
  declarations: [WebRowComponent],
  imports: [CommonModule, FlexLayoutModule],
  exports: [WebRowComponent]
})
export class WebRowModule {}
