import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WebRowComponent } from './web-row.component';

describe('WebRowComponent', () => {
  let component: WebRowComponent;
  let fixture: ComponentFixture<WebRowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [WebRowComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WebRowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
