import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'web-row',
  templateUrl: './web-row.component.html',
  styleUrls: ['./web-row.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class WebRowComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}
