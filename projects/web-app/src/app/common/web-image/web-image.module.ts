import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WebImageComponent } from './web-image.component';

@NgModule({
  declarations: [WebImageComponent],
  imports: [CommonModule],
  exports: [WebImageComponent]
})
export class WebImageModule {}
