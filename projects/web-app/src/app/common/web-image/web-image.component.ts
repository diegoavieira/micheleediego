import { Component, Input, OnInit } from '@angular/core';
import { DomSanitizer, SafeStyle } from '@angular/platform-browser';
import { WebImage } from './web-image';

@Component({
  selector: 'web-image',
  templateUrl: './web-image.component.html',
  styleUrls: ['./web-image.component.scss']
})
export class WebImageComponent implements OnInit {
  @Input() config: WebImage;

  urlImage: SafeStyle;

  constructor(private sanitizer: DomSanitizer) {}

  ngOnInit() {
    this.urlImage = this.sanitizer.bypassSecurityTrustStyle(`url(${this.config.src}`);

    if (this.config.tinged) {
      this.urlImage = this.sanitizer.bypassSecurityTrustStyle(`${this.config.tinged}, url(${this.config.src}`);
    }
  }
}
