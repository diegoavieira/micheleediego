export interface WebImage {
  src: string;
  width: string;
  height: string;
  minHeight?: string;
  tinged?: string;
  cover?: string;
  margin?: string;
}
