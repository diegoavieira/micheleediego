export interface WebButton {
  title: string;
  color?: string;
  margin?: string;
  width?: string;
  icon?: string;
  svgIcon?: string;
  action?: any;
}
