import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WebButtonComponent } from './web-button.component';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [WebButtonComponent],
  imports: [CommonModule, MatButtonModule, MatIconModule, RouterModule],
  exports: [WebButtonComponent]
})
export class WebButtonModule {}
