import { AfterViewInit, Component, ElementRef, Input, OnInit, Renderer2 } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { WebPalette } from '../web-theming/web-theming';
import { WebButton } from './web-button';

@Component({
  selector: 'web-button',
  templateUrl: './web-button.component.html',
  styleUrls: ['./web-button.component.scss']
})
export class WebButtonComponent implements OnInit, AfterViewInit {
  @Input() config: WebButton;
  @Input() disabled: boolean;
  @Input() type: 'button' | 'submit' | 'reset';

  constructor(
    private renderer: Renderer2,
    private el: ElementRef,
    private router: Router,
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer
  ) {}

  ngOnInit() {
    if (this.config.svgIcon) {
      this.matIconRegistry.addSvgIcon(
        this.config.svgIcon,
        this.domSanitizer.bypassSecurityTrustResourceUrl(`assets/svg/${this.config.svgIcon}.svg`)
      );
    }
  }

  ngAfterViewInit() {
    if (this.config.color in WebPalette) {
      this.renderer.addClass(this.el.nativeElement, this.config.color);
    } else {
      this.renderer.removeClass(this.el.nativeElement, this.config.color);
    }
  }

  onClick(action: any) {
    if (action) {
      if (action instanceof Function) {
        action();
      } else {
        this.router.navigate([action]);
      }
    }
  }
}
