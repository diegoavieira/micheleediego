import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WebSidenavComponent } from './web-sidenav.component';

describe('WebSidenavComponent', () => {
  let component: WebSidenavComponent;
  let fixture: ComponentFixture<WebSidenavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [WebSidenavComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WebSidenavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
