import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatSidenavModule } from '@angular/material/sidenav';
import { RouterModule } from '@angular/router';
import { WebSidenavComponent } from './web-sidenav.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatDividerModule } from '@angular/material/divider';

@NgModule({
  declarations: [WebSidenavComponent],
  imports: [CommonModule, RouterModule, MatSidenavModule, FlexLayoutModule, MatDividerModule],
  exports: [WebSidenavComponent]
})
export class WebSidenavModule {}
