import {
  Component,
  Input,
  OnInit,
  Output,
  EventEmitter,
  ViewEncapsulation,
  ContentChild,
  AfterViewInit
} from '@angular/core';
import { WebNavComponent } from '../web-nav/web-nav.component';

@Component({
  selector: 'web-sidenav',
  templateUrl: './web-sidenav.component.html',
  styleUrls: ['./web-sidenav.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class WebSidenavComponent implements OnInit, AfterViewInit {
  @Input() opened: boolean;
  @Input() isMobile: boolean;
  @Input() hidden: boolean;

  @Output() sidenavToggle = new EventEmitter();

  @ContentChild(WebNavComponent) nav: WebNavComponent;

  constructor() {}

  ngOnInit() {}

  ngAfterViewInit() {
    if (this.nav) {
      this.nav.sidenavToggle.subscribe(() => {
        this.sidenavToggle.emit();
      });
    }
  }

  onToggleSidenav() {
    this.sidenavToggle.emit();
  }
}
