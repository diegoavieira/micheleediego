import {
  Component,
  Input,
  OnInit,
  Output,
  EventEmitter,
  ViewEncapsulation,
  AfterViewInit,
  ElementRef,
  Renderer2
} from '@angular/core';
import { WebPalette } from '../web-theming/web-theming';
import { WebNav } from './web-nav';

@Component({
  selector: 'web-nav',
  templateUrl: './web-nav.component.html',
  styleUrls: ['./web-nav.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class WebNavComponent implements OnInit, AfterViewInit {
  @Input() config: WebNav;

  @Output() sidenavToggle = new EventEmitter();

  constructor(private renderer: Renderer2, private el: ElementRef) {}

  ngOnInit() {
    if (this.config.items && this.config.items.length) {
      this.config.items = this.config.items.filter((n) => !n.hidden);
    }
  }

  ngAfterViewInit() {
    if (this.config.activeColor in WebPalette) {
      this.renderer.addClass(this.el.nativeElement, this.config.activeColor);
    } else {
      this.renderer.removeClass(this.el.nativeElement, this.config.activeColor);
    }
  }

  onToggleSidenav() {
    this.sidenavToggle.emit();
  }
}
