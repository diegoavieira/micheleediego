interface Items {
  title: string;
  url: string;
  hidden?: boolean;
}

export interface WebNav {
  items: Items[];
  vertical?: boolean;
  activeColor?: string;
}
