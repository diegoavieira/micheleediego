import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WebNavComponent } from './web-nav.component';
import { RouterModule } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({
  declarations: [WebNavComponent],
  imports: [CommonModule, RouterModule, MatButtonModule, FlexLayoutModule],
  exports: [WebNavComponent]
})
export class WebNavModule {}
