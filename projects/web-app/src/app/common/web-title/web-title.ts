export interface WebTitle {
  title: string;
  color?: string;
  fontWeight?: string;
  fontStyle?: string;
  margin?: string;
  textAlign?: string;
  isSubtitle?: boolean;
}
