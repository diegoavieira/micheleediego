import { AfterViewInit, Component, ElementRef, Input, OnInit, Renderer2, ViewEncapsulation } from '@angular/core';
import { WebPalette } from '../web-theming/web-theming';
import { WebTitle } from './web-title';

@Component({
  selector: 'web-title',
  templateUrl: './web-title.component.html',
  styleUrls: ['./web-title.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class WebTitleComponent implements OnInit, AfterViewInit {
  @Input() config: WebTitle;

  constructor(private renderer: Renderer2, private el: ElementRef) {}

  ngOnInit() {}

  ngAfterViewInit() {
    if (this.config.color in WebPalette) {
      this.renderer.addClass(this.el.nativeElement, this.config.color);
    } else {
      this.renderer.removeClass(this.el.nativeElement, this.config.color);
    }
  }
}
