import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WebTitleComponent } from './web-title.component';

@NgModule({
  declarations: [WebTitleComponent],
  imports: [CommonModule],
  exports: [WebTitleComponent]
})
export class WebTitleModule {}
