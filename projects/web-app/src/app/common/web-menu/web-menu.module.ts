import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WebMenuComponent } from './web-menu.component';
import { MatMenuModule } from '@angular/material/menu';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  declarations: [WebMenuComponent],
  imports: [CommonModule, MatMenuModule, MatButtonModule],
  exports: [WebMenuComponent]
})
export class WebMenuModule {}
