import { Observable } from 'rxjs';

export interface WebMenuItem {
  title: string;
  action?: () => void;
  hidden$?: Observable<boolean>;
}

export interface WebMenu {
  items: WebMenuItem[];
  color?: string;
  disabled?: boolean;
}
