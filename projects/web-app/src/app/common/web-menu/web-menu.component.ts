import {
  AfterViewInit,
  Component,
  ElementRef,
  Input,
  OnInit,
  Renderer2,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import { MatMenuTrigger } from '@angular/material/menu';
import { WebPalette } from '../web-theming/web-theming';
import { WebMenu, WebMenuItem } from './web-menu';

@Component({
  selector: 'web-menu',
  templateUrl: './web-menu.component.html',
  styleUrls: ['./web-menu.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class WebMenuComponent implements OnInit, AfterViewInit {
  @Input() config: WebMenu;

  @ViewChild(MatMenuTrigger, { static: true }) trigger: MatMenuTrigger;

  constructor(private renderer: Renderer2, private el: ElementRef) {}

  ngOnInit() {}

  ngAfterViewInit() {
    if (this.config.color in WebPalette) {
      this.renderer.addClass(this.el.nativeElement, this.config.color);
    } else {
      this.renderer.removeClass(this.el.nativeElement, this.config.color);
    }
  }

  openMenu() {
    if (!this.config.disabled) {
      this.trigger.openMenu();
    }
  }

  action(item: WebMenuItem) {
    if (item.action && item.action instanceof Function) {
      item.action();
    }
  }
}
