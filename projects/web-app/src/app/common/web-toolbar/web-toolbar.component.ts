import {
  Component,
  Input,
  OnInit,
  Output,
  EventEmitter,
  ViewEncapsulation,
  AfterViewInit,
  Renderer2,
  ElementRef
} from '@angular/core';
import { WebPalette } from '../web-theming/web-theming';

@Component({
  selector: 'web-toolbar',
  templateUrl: './web-toolbar.component.html',
  styleUrls: ['./web-toolbar.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class WebToolbarComponent implements OnInit, AfterViewInit {
  @Input() hasSidenav: boolean;
  @Input() hasNav: boolean;
  @Input() isMobile: boolean;
  @Input() title: string;
  @Input() color: string;

  @Output() sidenavToggle = new EventEmitter();

  constructor(private renderer: Renderer2, private el: ElementRef) {}

  ngOnInit() {}

  ngAfterViewInit() {
    if (this.color in WebPalette) {
      this.renderer.addClass(this.el.nativeElement, this.color);
    } else {
      this.renderer.removeClass(this.el.nativeElement, this.color);
    }
  }

  onToggleSidenav() {
    this.sidenavToggle.emit();
  }
}
