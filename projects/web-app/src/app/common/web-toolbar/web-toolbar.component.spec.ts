import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WebToolbarComponent } from './web-toolbar.component';

describe('WebToolbarComponent', () => {
  let component: WebToolbarComponent;
  let fixture: ComponentFixture<WebToolbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [WebToolbarComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WebToolbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
