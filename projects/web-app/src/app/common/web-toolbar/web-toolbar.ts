export interface WebToolbar {
  background?: string;
  text?: string;
}
