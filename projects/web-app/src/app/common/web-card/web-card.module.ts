import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WebCardComponent } from './web-card.component';
import { MatCardModule } from '@angular/material/card';
import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({
  declarations: [WebCardComponent],
  imports: [CommonModule, MatCardModule, FlexLayoutModule],
  exports: [WebCardComponent]
})
export class WebCardModule {}
