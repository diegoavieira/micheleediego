import { ElementRef } from '@angular/core';
import { Renderer2 } from '@angular/core';
import { AfterViewInit, Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { WebPalette } from '../web-theming/web-theming';
import { WebCard } from './web-card';

@Component({
  selector: 'web-card',
  templateUrl: './web-card.component.html',
  styleUrls: ['./web-card.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class WebCardComponent implements OnInit, AfterViewInit {
  @Input() config: WebCard = {};

  constructor(private renderer: Renderer2, private el: ElementRef) {}

  ngOnInit() {}

  ngAfterViewInit() {
    if (this.config.color in WebPalette) {
      this.renderer.addClass(this.el.nativeElement, this.config.color);
    } else {
      this.renderer.removeClass(this.el.nativeElement, this.config.color);
    }
  }
}
