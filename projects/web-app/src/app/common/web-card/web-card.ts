export interface WebCard {
  title?: string;
  subtitle?: string;
  color?: string;
  headerAlign?: string;
}
