import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WebTabsComponent } from './web-tabs.component';
import { MatTabsModule } from '@angular/material/tabs';
import { RouterModule } from '@angular/router';
import { MatDividerModule } from '@angular/material/divider';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  declarations: [WebTabsComponent],
  imports: [CommonModule, MatTabsModule, RouterModule, MatDividerModule, MatButtonModule],
  exports: [WebTabsComponent]
})
export class WebTabsModule {}
