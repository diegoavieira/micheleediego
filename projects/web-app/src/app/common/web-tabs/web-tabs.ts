interface Tab {
  title: string;
  url: string;
}

export interface WebTabs {
  tabs: Tab[];
  activeColor?: string;
}
