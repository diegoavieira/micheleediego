import { AfterViewInit, Component, ElementRef, Input, OnInit, Renderer2, ViewEncapsulation } from '@angular/core';
import { WebPalette } from '../web-theming/web-theming';
import { WebTabs } from './web-tabs';

@Component({
  selector: 'web-tabs',
  templateUrl: './web-tabs.component.html',
  styleUrls: ['./web-tabs.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class WebTabsComponent implements OnInit, AfterViewInit {
  @Input() config: WebTabs;

  constructor(private renderer: Renderer2, private el: ElementRef) {}

  ngOnInit() {}

  ngAfterViewInit() {
    if (this.config.activeColor in WebPalette) {
      this.renderer.addClass(this.el.nativeElement, this.config.activeColor);
    } else {
      this.renderer.removeClass(this.el.nativeElement, this.config.activeColor);
    }
  }
}
