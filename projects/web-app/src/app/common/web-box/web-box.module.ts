import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WebBoxComponent } from './web-box.component';
import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({
  declarations: [WebBoxComponent],
  imports: [CommonModule, FlexLayoutModule],
  exports: [WebBoxComponent]
})
export class WebBoxModule {}
