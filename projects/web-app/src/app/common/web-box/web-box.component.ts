import { AfterViewInit, Component, ElementRef, Input, OnInit, Renderer2, ViewEncapsulation } from '@angular/core';
import { WebPalette } from '../web-theming/web-theming';
import { WebBox } from './web-box';

@Component({
  selector: 'web-box',
  templateUrl: './web-box.component.html',
  styleUrls: ['./web-box.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class WebBoxComponent implements OnInit, AfterViewInit {
  @Input() config: WebBox = {};

  constructor(private renderer: Renderer2, private el: ElementRef) {}

  ngOnInit() {}

  ngAfterViewInit() {
    if (this.config.color in WebPalette) {
      this.renderer.addClass(this.el.nativeElement, this.config.color);
    } else {
      this.renderer.removeClass(this.el.nativeElement, this.config.color);
    }
  }
}
