import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WebBoxComponent } from './web-box.component';

describe('WebBoxComponent', () => {
  let component: WebBoxComponent;
  let fixture: ComponentFixture<WebBoxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [WebBoxComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WebBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
