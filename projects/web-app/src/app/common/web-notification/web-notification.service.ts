import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig, MatSnackBarRef, SimpleSnackBar } from '@angular/material/snack-bar';
import { WebNotification } from './web-notification';

@Injectable()
export class WebNotificationService {
  snackBarRef: MatSnackBarRef<SimpleSnackBar>;

  constructor(private snackBar: MatSnackBar) {}

  open(config: WebNotification) {
    const snackBarConfig: MatSnackBarConfig = {
      duration: config.notAutoClose ? null : 3000,
      horizontalPosition: 'right',
      panelClass: ['web-notification']
    };

    this.snackBarRef = this.snackBar.open(config.message, config.label, snackBarConfig);

    if (config.action && config.action instanceof Function) {
      this.snackBarRef.onAction().subscribe(() => {
        config.action();
      });
    }
  }

  close() {
    this.snackBarRef.dismiss();
  }
}
