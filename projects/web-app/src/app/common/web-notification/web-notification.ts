export interface WebNotification {
  message: string;
  label?: string;
  action?: () => void;
  notAutoClose?: boolean;
}
