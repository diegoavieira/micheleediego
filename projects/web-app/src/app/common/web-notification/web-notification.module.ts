import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WebNotificationService } from './web-notification.service';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  declarations: [],
  imports: [CommonModule, MatSnackBarModule, MatButtonModule],
  exports: [],
  providers: [WebNotificationService]
})
export class WebNotificationModule {}
