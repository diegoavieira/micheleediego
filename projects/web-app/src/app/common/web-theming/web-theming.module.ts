import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WebThemingDirective } from './web-theming.directive';

@NgModule({
  declarations: [WebThemingDirective],
  imports: [CommonModule],
  exports: [WebThemingDirective]
})
export class WebThemingModule {}
