export interface WebTheming {
  primary: string;
  accent: string;
  warn?: string;
  darker?: string;
  lighter?: string;
  background?: string;
}

export enum WebPalette {
  'web-primary',
  'web-accent',
  'web-warn',
  'web-darker',
  'web-lighter'
}
