import { Observable } from 'rxjs';

export interface WebAvatar {
  account$?: Observable<Account>;
  height?: string;
  width?: string;
}
