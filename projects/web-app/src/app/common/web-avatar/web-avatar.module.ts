import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WebAvatarComponent } from './web-avatar.component';
import { MatButtonModule } from '@angular/material/button';
import { WebImageModule } from '../web-image/web-image.module';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
  declarations: [WebAvatarComponent],
  imports: [CommonModule, WebImageModule, MatButtonModule, MatIconModule],
  exports: [WebAvatarComponent]
})
export class WebAvatarModule {}
