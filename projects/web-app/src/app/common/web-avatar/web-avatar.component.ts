import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { WebAvatar } from './web-avatar';

@Component({
  selector: 'web-avatar',
  templateUrl: './web-avatar.component.html',
  styleUrls: ['./web-avatar.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class WebAvatarComponent implements OnInit {
  @Input() config: WebAvatar = {};

  constructor() {}

  ngOnInit() {}
}
