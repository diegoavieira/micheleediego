import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WebAvatarComponent } from './web-avatar.component';

describe('WebAvatarComponent', () => {
  let component: WebAvatarComponent;
  let fixture: ComponentFixture<WebAvatarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WebAvatarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WebAvatarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
