import { AfterViewInit, Component, ElementRef, Input, OnInit, Renderer2 } from '@angular/core';
import { WebPalette } from '../web-theming/web-theming';
import { WebText } from './web-text';

@Component({
  selector: 'web-text',
  templateUrl: './web-text.component.html',
  styleUrls: ['./web-text.component.scss']
})
export class WebTextComponent implements OnInit, AfterViewInit {
  @Input() config: WebText;

  constructor(private renderer: Renderer2, private el: ElementRef) {}

  ngOnInit() {}

  ngAfterViewInit() {
    if (this.config.color in WebPalette) {
      this.renderer.addClass(this.el.nativeElement, this.config.color);
    } else {
      this.renderer.removeClass(this.el.nativeElement, this.config.color);
    }
  }
}
