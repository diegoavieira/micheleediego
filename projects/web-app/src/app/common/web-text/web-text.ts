export interface WebText {
  texts: string[];
  color?: string;
}
