import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WebFooterComponent } from './web-footer.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatIconModule } from '@angular/material/icon';
import { MatDividerModule } from '@angular/material/divider';
import { WebSvgModule } from '../web-svg/web-svg.module';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [WebFooterComponent],
  imports: [CommonModule, FlexLayoutModule, MatIconModule, MatDividerModule, WebSvgModule, RouterModule],
  exports: [WebFooterComponent]
})
export class WebFooterModule {}
