# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.0.18](https://gitlab.com/diegoavieira/micheleediego/compare/v0.0.16...v0.0.18) (2022-10-03)

**Note:** Version bump only for package @micheleediego/web

## [0.0.17](https://gitlab.com/diegoavieira/micheleediego/compare/v0.0.16...v0.0.17) (2022-10-03)

**Note:** Version bump only for package @micheleediego/web

## [0.0.16](https://gitlab.com/diegoavieira/micheleediego/compare/v0.0.15...v0.0.16) (2022-09-21)

### Bug Fixes

- adjustments ([241e91c](https://gitlab.com/diegoavieira/micheleediego/commit/241e91cbaa8f0219e500b91b9d21aef5b9888969))

## [0.0.15](https://gitlab.com/diegoavieira/micheleediego/compare/v0.0.14...v0.0.15) (2022-04-29)

### Bug Fixes

- adjustments ([98fbbc8](https://gitlab.com/diegoavieira/micheleediego/commit/98fbbc8a625880719ff06bf8718c9dfe99ebd5a9))

## [0.0.14](https://gitlab.com/diegoavieira/micheleediego/compare/v0.0.13...v0.0.14) (2022-04-12)

### Bug Fixes

- adjustments ([17ded5b](https://gitlab.com/diegoavieira/micheleediego/commit/17ded5b68d8a55207ccd4f9e02b17d2ab88caab6))
- adjustments ([4935324](https://gitlab.com/diegoavieira/micheleediego/commit/493532464ee60a7baabd92f1abf924c04543edcf))

## [0.0.13](https://gitlab.com/diegoavieira/micheleediego/compare/v0.0.12...v0.0.13) (2021-04-25)

### Bug Fixes

- changed rsvp date ([461c25c](https://gitlab.com/diegoavieira/micheleediego/commit/461c25c974e2f6c69707327eda08c4fc2a5d744c))

## [0.0.12](https://gitlab.com/diegoavieira/micheleediego/compare/v0.0.11...v0.0.12) (2021-04-25)

### Bug Fixes

- changed date ([61a7e6e](https://gitlab.com/diegoavieira/micheleediego/commit/61a7e6e72c197a792f9c97276073aaafc5fadf99))

## [0.0.11](https://gitlab.com/diegoavieira/micheleediego/compare/v0.0.10...v0.0.11) (2020-11-13)

**Note:** Version bump only for package @micheleediego/web

## [0.0.10](https://gitlab.com/diegoavieira/micheleediego/compare/v0.0.9...v0.0.10) (2020-11-12)

**Note:** Version bump only for package @micheleediego/web

## [0.0.9](https://gitlab.com/diegoavieira/micheleediego/compare/v0.0.8...v0.0.9) (2020-11-11)

**Note:** Version bump only for package @micheleediego/web

## [0.0.8](https://gitlab.com/diegoavieira/micheleediego/compare/v0.0.7...v0.0.8) (2020-11-09)

**Note:** Version bump only for package @micheleediego/web

## [0.0.7](https://gitlab.com/diegoavieira/micheleediego/compare/v0.0.6...v0.0.7) (2020-11-09)

**Note:** Version bump only for package @micheleediego/web

## [0.0.6](https://gitlab.com/diegoavieira/micheleediego/compare/v0.0.5...v0.0.6) (2020-11-09)

**Note:** Version bump only for package @micheleediego/web

## [0.0.5](https://gitlab.com/diegoavieira/micheleediego/compare/v0.0.4...v0.0.5) (2020-11-06)

### Bug Fixes

- adjustments ([ecce3fd](https://gitlab.com/diegoavieira/micheleediego/commit/ecce3fdc0febf194f422f516485f0debac770e01))
- adjustments ([c435548](https://gitlab.com/diegoavieira/micheleediego/commit/c435548c41edf6f5a2f0f55ad838c7fa093c38c5))

## [0.0.4](https://gitlab.com/diegoavieira/micheleediego/compare/v0.0.3...v0.0.4) (2020-11-05)

**Note:** Version bump only for package @micheleediego/web

## [0.0.3](https://gitlab.com/diegoavieira/micheleediego/compare/v0.0.2...v0.0.3) (2020-10-29)

**Note:** Version bump only for package @micheleediego/web

**Note:** Version bump only for package @micheleediego/web

## [0.0.2](https://gitlab.com/diegoavieira/micheleediego/compare/v0.0.1...v0.0.2) (2020-10-29)

**Note:** Version bump only for package @micheleediego/web

## 0.0.1 (2020-10-29)

**Note:** Version bump only for package @micheleediego/web
